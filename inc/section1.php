<div class="site-section" id="section-1">
    <div class="container">
        <div class="row">
            <div class="col-12 pb-5 mb-lg-0">
                <div class="row py-5">
                    <div class="col-4 iconesHome"><img src="<?= $url; ?>images/icone1.png" alt="icone 1"
                            class="img-fluid">
                        <p style="font-size: 1.2em;" class="m-0">Compare Preços</p>
                    </div>

                    <div class="col-4 iconesHome"><img src="<?= $url; ?>images/icone2.png" alt="icone 2"
                            class="img-fluid">
                        <p style="font-size: 1.2em;" class="m-0">Agilidade e simplicidade</p>
                    </div>

                    <div class="col-4 iconesHome"><img src="<?= $url; ?>images/icone3.png" alt="icone 3"
                            class="img-fluid">
                        <p style="font-size: 1.2em;" class="m-0">Cote com Diversas Empresas</p>
                    </div>
                </div>
            </div>
        </div>
    </div>