import re
import os
# Variável para armazenar o conteúdo a ser adicionado
content_to_add = ""

def writearchive(message):
    """Acumula mensagens em uma variável global."""
    global content_to_add
    content_to_add += message + "\n"  # Adiciona uma nova linha a cada mensagem

def cleararchive():
    global content_to_add
    content_to_add = ""

def insertarchive(filepath,path):
    """Insere o conteúdo acumulado no arquivo, abaixo de uma linha específica."""
    try:
        with open(filepath, 'r', encoding='utf-8') as file:
            lines = file.readlines()
        
        # Encontrar o índice da linha onde o conteúdo deve ser inserido
        index = -1
        for i, line in enumerate(lines):
            if path in line:
                index = i + 1
                break
        
        if index == -1:
            print("Tag <meta charset='utf-8'> não encontrada.")
            return

        # Inserir o conteúdo acumulado após a lina especificada
        lines.insert(index, content_to_add)

        # Escrever tudo de volta ao arquivo
        with open(filepath, 'w', encoding='utf-8') as file:
            file.writelines(lines)

        print("Conteúdo inserido com sucesso.")
    except FileNotFoundError:
        print(f"O arquivo {filepath} não foi encontrado.")
    except Exception as e:
        print(f"Ocorreu um erro: {str(e)}")

def searchscript(file_paths, parametros):
    script_pattern = re.compile(r"<script\b[^>]*>(?:(?!<\/script>).)*?</script>", re.I | re.DOTALL)
    
    # Verifica se é uma lista, se não, transforma em lista
    if not isinstance(file_paths, list):
        file_paths = [file_paths]

    for file_path in file_paths:
        if not os.path.exists(file_path):
            print(f"O arquivo {file_path} não existe.")
            continue

        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                contents = file.read()
            
            modified_contents = contents
            scripts_found = script_pattern.findall(contents)

            for script in scripts_found:
                if any(re.search(re.escape(param), script, re.I) for param in parametros):
                    if 'jquery-' in script: 
                        jqueryex = os.path.exists("jquery.php")
                        if jqueryex:
                            modified_contents = modified_contents.replace(script, "")
                            writearchive('<?php include "inc/jquery.php"?>')
                            break
                    if 'slicknav' in script:
                        modified_contents = modified_contents.replace(script, "")
                        writearchive('<script><? if($isMobile){ include "js/jquery.slicknav.js"; } ?> </script>')
                        break
                    modified_contents = modified_contents.replace(script, "")
                    # Adiciona o script removido ao arquivo de log (assegure-se de que esta função esteja definida)
                    writearchive(script)
            
            with open(file_path, 'w', encoding='utf-8') as file:
                file.write(modified_contents)
            # print(f"Modificações salvas em {file_path}")
        
        except Exception as e:
            print(f"Ocorreu um erro ao processar o arquivo {file_path}: {e}")


def search_links(file_paths, search_parameters, terms_to_include):
    link_pattern = re.compile(r'<link\b[^>]*>', re.I | re.DOTALL)

    for path in file_paths:
        if not os.path.exists(path):
            continue

        with open(path, 'r+', encoding='utf-8') as file:
            content = file.read()
            matches = link_pattern.findall(content)

            for match in matches:
                if any(term in match for term in terms_to_include):
                    if 'fontawesome' in match:
                            new_link = '<link rel="preload" as="style" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" onload="this.rel=\'stylesheet\'">'
                            content = content.replace(match, new_link)
                            file.seek(0)
                            file.write(content)
                            file.truncate()
                            break  
                    if 'fancyapps' in match:
                            content = content.replace(match, '')
                            writearchive(match)
                            file.seek(0)
                            file.write(content)
                            file.truncate()
                            break
                            
                    href_match = re.search(r'href="([^"]*)"', match)
                    rel_match = re.search(r'rel="([^"]*)"', match)
                    if href_match and rel_match:
                        href = href_match.group(1)
                        rel = rel_match.group(1)
                        if rel in search_parameters:
                            if rel in ['preload', 'preconnect']:
                                content = content.replace(match, '')
                                file.seek(0)
                                file.write(content)
                                file.truncate()
                            elif rel == 'stylesheet':
                                

                                content = content.replace(match, '')
                                new_include = f'<style><?php include "{href}" ?></style>'
                                writearchive(new_include)

def deletescript(file_paths, parametros):
    script_pattern = re.compile(r"<script\b[^>]*>(?:(?!<\/script>).)*?</script>", re.I | re.DOTALL)
    
    # Verifica se é uma lista, se não, transforma em lista
    if not isinstance(file_paths, list):
        file_paths = [file_paths]

    for file_path in file_paths:
        if not os.path.exists(file_path):
            print(f"O arquivo {file_path} não existe.")
            continue

        try:
            with open(file_path, 'r', encoding='utf-8') as file:
                contents = file.read()
            
            modified_contents = contents
            scripts_found = script_pattern.findall(contents)
            # print(f"Encontrados {len(scripts_found)} scripts em {file_path}")

            scripts_to_delete = [script for script in scripts_found if any(re.search(re.escape(param), script, re.I) for param in parametros)]
            # print(f"Serão deletados {len(scripts_to_delete)} scripts em {file_path}")
            
            for script in scripts_to_delete:
                if "GoogleAnalyticsObject" in script:
                    writearchive('<script><?php include "js/geral.js"; include "js/app.js"?></script> ')
                modified_contents = modified_contents.replace(script, "")

            with open(file_path, 'w', encoding='utf-8') as file:
                file.write(modified_contents)
            # print(f"Modificações salvas em {file_path}")
        
        except Exception as e:
            print(f"Ocorreu um erro ao processar o arquivo {file_path}: {e}")


def writefooter(arquivo, conteudo):
    try:
        # Abrir o arquivo no modo de adição (append)
        with open(arquivo, 'a') as file:
            # Escrever o conteúdo no final do arquivo
            file.write(conteudo)
            # print("Conteúdo escrito com sucesso no final do arquivo.")
    except Exception as e:
        print(f"Não foi possível abrir o arquivo. Erro: {e}")

def movedivexit(arquivo):
     with open(arquivo, 'r+', encoding='utf-8') as file:
            div = '''<div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>'''
            content = file.read()
            content = content.replace(div, '')
            file.seek(0)
            file.write(content)
            file.truncate()
            writearchive(div)


def remove_fancy_php_from_file(file_paths,entrada):
    for file_path in file_paths:
        if entrada == "fancy":
            regex = r'<\?php[\s\S]*?fancy\.php[\s\S]*?\?>'
            with open(file_path, 'r', encoding='utf-8') as file:
                content = file.read()
            
            new_content = re.sub(regex, '', content)
            
            with open(file_path, 'w', encoding='utf-8') as file:
                file.write(new_content)

        if entrada == "LAB":
            regex = r'''<\?(?:php)?\s*include\s*\(\s*['"]inc\/LAB\.php['"]\s*\)\s*;\s*\?>'''
            with open(file_path, 'r', encoding='utf-8') as file:
                content = file.read()
            
            new_content = re.sub(regex, '', content)
            
            with open(file_path, 'w', encoding='utf-8') as file:
                file.write(new_content)

    if entrada == "fancy": writearchive('<? include "inc/fancy.php"?>')
    if entrada == "LAB": writearchive('<? include "inc/LAB.php"?>')
                

def remove_shrark_php_from_file(file_paths):
    for file_path in file_paths:
        regex = '<div id="sharkOrcamento" style="display: none;"></div>'
        with open(file_path, 'r', encoding='utf-8') as file:
            content = file.read()
        
        new_content = re.sub(regex, '', content)
        
        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(new_content)
# Uso da função
def ismobile(file_path, text_to_append):
    with open(file_path, 'a', encoding='utf-8') as file:
        file.write(text_to_append + "\n")
    print(f"Is Mobile adicionado na {file_path}")

def replace_paths_in_css(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read()

    # Substitui '../' por '' usando expressões regulares
    modified_content = re.sub(r'\.\./', '', content)

    # Abre o arquivo para escrita e salva o conteúdo modificado
    with open(file_path, 'w', encoding='utf-8') as file:
        file.write(modified_content)


# Variaveis e chamdas das funções
styles = ["style.css","css/normalize.css"]
fontaw = ["fontawesome.com"]
jquery = ["jquery-"]
lazysize = ["lazysizes.min.js"]

slicknav = ["slicknav"]
mordenizer = ["modernizr"]
rels = ["preload","preconnect","stylesheet"]
scripts = ["jquery-","lazysizes.min.js"]
# Footer
searcharchivess = ["head.php", "footer.php"]
details = ["scrollUp","click-actions","leiaMais", "readmore","js/app.js","js/geral.js"]
package = ["package.js","botao-cotar"]
asidelauncher = ["js/sdk/install.js", "const aside"]
chat = ["jivosite"]
asideexit = ["exit-banner-div", "js.cookie.min.js", "fancybox/fancybox.umd.js", "js/sdk/install.js","exit-banner-container"]
tagmanager = ["gtag"]
dltscript = ["window.performance","GoogleAnalyticsObject"]
fancystyle = ["fancyapps"]
varismobile = '<? $isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]); ?>'
filepath = 'head.php'
footerref = "<!-- Pagespeed.py -->"

# Head

writearchive("<!-- Includes Principais -->")
searchscript(searcharchivess, jquery)

writearchive("<!-- Styles  e lazysizes-->")
search_links(searcharchivess, rels, styles)

writearchive("<!-- lazysizes-->")
searchscript(searcharchivess, lazysize)
remove_fancy_php_from_file(searcharchivess, "fancy")

writearchive("<!-- slicknav -->")
searchscript(searcharchivess, slicknav)
searchscript(searcharchivess, mordenizer)

writearchive("<!-- fontawesome -->")
search_links(searcharchivess, rels, fontaw)

insertarchive(filepath, "inc/geral.php")

cleararchive()

# Footer

filepath = 'footer.php'
writefooter(filepath, footerref)

writearchive("")
writearchive("<!-- Includes Detalhes -->")
searchscript(searcharchivess, details)
deletescript(searcharchivess, dltscript)

writearchive("")
writearchive("<!-- API botao-cotar -->")
searchscript(searcharchivess, package)
remove_shrark_php_from_file(searcharchivess)

writearchive("")
writearchive("<!-- Script Aside Launcher -->")
searchscript(searcharchivess, asidelauncher)

writearchive("")
writearchive("<!-- Chat -->")
searchscript(searcharchivess, chat)

writearchive("")
writearchive("<!-- Launcher de exit -->")
movedivexit("footer.php")
search_links(searcharchivess, rels, fancystyle)
searchscript(searcharchivess, asideexit)

writearchive("<!-- Outros scripts -->")
remove_fancy_php_from_file(searcharchivess, "LAB")


writearchive("")
writearchive("<!-- TagManager -->")
searchscript(searcharchivess, tagmanager)

ismobile("geral.php", varismobile)

insertarchive("footer.php", footerref)
replace_paths_in_css("../css/style.css")