<div id="carousel" class="carousel slide" data-ride="carousel">    
    <div class="carousel-inner">
        <div class="carousel-item active one">	
            <div class="d-flex align-items-center w-100 h-100" style="background:rgba(0,0,0,.4);">
                <div class="container azul">
                    <div class="row justify-content-center ">
                        <div class="col-md-6 col-9 text-center border slider_text">
                            <h2>ESPUMA DE COLCHÃO</h2>
                            <p>A espuma de colchão é um dos materiais mais importantes com o alvo de proporcionar conforto na hora de dormir.</p>
                            <a href="<?= $url; ?>espuma-de-colchao" class="btn btn-lg theme-btn">Saiba mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item two">	
            <div class="d-flex align-items-center w-100 h-100" style="background:rgba(0,0,0,.4);">
                <div class="container azul">
                    <div class="row justify-content-center ">
                        <div class="col-md-6 col-9 text-center border slider_text">
                            <h2>ESPUMA EM BLOCO</h2>
                            <p>São diversos os modelos de engrenagens existentes no mercado. Estes dispositivos são utilizados nos sistemas de transmissão em muitas aplicações diferentes.</p>
                            <a href="<?= $url; ?>espuma-em-bloco" class="btn btn-lg theme-btn">Saiba mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item three">	
            <div class="d-flex align-items-center w-100 h-100" style="background:rgba(0,0,0,.4);">
                <div class="container azul">
                    <div class="row justify-content-center ">
                        <div class="col-md-6 col-9 text-center border slider_text">
                            <h2>ESPUMAS TÉCNICAS</h2>
                            <p>Nem sempre as espumas técnicas de poliuretano são percebidas ou identificadas, já que variadas vezes ela fica no interior do produto, fazendo seu preenchimento.</p>
                            <a href="<?= $url; ?>espumas-tecnicas" class="btn btn-lg theme-btn">Saiba mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>