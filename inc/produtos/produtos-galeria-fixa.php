
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>

<?php $lista = array('Flocos de isopor', 'Flocos de isopor para salgadinho', 'Flocos de isopor para embalagem', 'Flocos de isopor para proteção', 'Isopor para embalagem', 'Flocos de isopor onde comprar', 'Proteção de isopor para embalagem', 'Isopor para preenchimento de embalagens', 'Flocos de isopor comprar', 'Flocos de isopor venda', 'Flocos para preencher caixa', 'Flocos para preenchimento de embalagem', 'Flocos de preenchimento');
                                shuffle($lista);
                                for ($i = 1; $i < 13; $i++) { ?>  
<?php
$folder = "produtos";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/produtos/produtos-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-22"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/produtos/produtos-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/produtos/thumbs/produtos-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>