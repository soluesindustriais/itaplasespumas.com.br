<?php include('inc/geral.php'); ?>
<!-- Includes Principais -->
<script src="<?= $url ?>js/jquery-3.3.1.min.js"></script>
<!-- Styles  e lazysizes-->
<link rel="preload" as="style" href="<?=$url?>css/style.css">
<link rel="stylesheet" href="<?=$url?>css/style.css">
<!-- lazysizes-->
<script src="js/lazysizes.min.js"> </script>
<? include "inc/fancy.php"?>
<!-- slicknav -->
<!-- fontawesome -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="<?=$url?>images/favicon.png">
<title><?= ucfirst($title)." - ".$nomeSite; ?></title>

<link rel="preconnect" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900"> 
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900"> 

<link rel="preload" as="style" href="<?=$url?>css/bootstrap.min.css"> 
<link rel="stylesheet" href="<?=$url?>css/bootstrap.min.css"> 

<link rel="preload" as="style" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" onload="this.rel='stylesheet'">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<?php  if ($h1 == "Informações"){?>
<link rel="preload" as="style" href="<?=$url?>css/jquery.paginate.css">
<link rel="stylesheet" href="<?=$url?>css/jquery.paginate.css">
<?php } ?>
<?php if ($title == "Home") { ?>
<link rel="preload" as="style" href="<?=$url?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<?=$url?>css/owl.carousel.min.css">

<link rel="preload" as="style" href="<?=$url?>css/owl.theme.default.min.css">
<link rel="stylesheet" href="<?=$url?>css/owl.theme.default.min.css">
<?php } ?>




<base href="<?=$url;?>">
<meta name="description" content="<?=ucfirst($desc)?>">
<meta name="keywords" content="<?=str_replace($prepos,', ', $h1).', '.$nomeSite?>">
<meta name="geo.position" content="<?=$latitude.";".$longitude?>">
<meta name="geo.placename" content="<?=$cidade."-".$uf?>">
<meta name="geo.region" content="<?=$uf?>-BR">
<meta name="ICBM" content="<?=$latitude.",".$longitude?>">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="<?=$url.$urlPagina?>">
<meta name="author" content="<?=$nomeSite?>">
<link rel="shortcut icon" href="<?=$url?>images/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="<?=$title." - ".$nomeSite?>">
<meta property="og:type" content="article">
<meta property="og:image" content="<?=$url?>images/logo.png">
<meta property="og:url" content="<?=$url.$urlPagina?>">
<meta property="og:description" content="<?=$desc?>">
<meta property="og:site_name" content="<?=$nomeSite?>">

<!-- DE - Logo -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "Organization",
    "url": "<?=$url?>",
    "logo": "<?=$url?>images/logo.png"
}
</script>
<?php if(isset($urlPagInterna) && ($urlPagInterna !="")){ ?>
<!-- DE - Artigo -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "Article",
    "image": "<?= $url.'imagens/informacoes/'.$urlPagina; ?>-01.jpg",
    "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "<?= $url; ?>"
    },
    "headline": "<?= $h1; ?>",
    "description": "<?= $desc; ?>",
    "dateModified": "2019-10-01"
    "datePublished": "2019-10-01",
    "author": {
        "@type": "Thing",
        "name": "<?= $nomeSite; ?>"
    },
    "publisher": {
        "@type": "Organization",
        "name": "<?= $nomeSite; ?>",
        "logo": {
            "@type": "ImageObject",
            "url": "<?= $url; ?>images/logo.png",
            "width": "180",
            "height": "60"
        }
    }
}
</script>
<?php } ?>

