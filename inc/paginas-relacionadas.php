<div class="col-12 mt-4">
<h2 class=" col-12 mb-3">Publicações Relacionadas</h2>
<div class="row" style="display: flex; justify-content: space-between;">
	 <?php
		$limit = 6;
		include('inc/classes/strFuncoesMPI.class.php'); 
		$str = new strFuncoes();
		shuffle($vetKey);
		$strPalavra = implode(" ", $str->RetiraPreposicao($h1));
		$vetKey_PR = $str->buscaVetorAprox($strPalavra, $vetKey, $limit, $h1);
			
		for ($i = 0; $i < sizeof($vetKey_PR); $i++) {
	?>
	<a rel="nofollow" href="<?=$url?><?=$vetKey[$vetKey_PR[$i]]['url']; ?>" title="<?=$vetKey[$vetKey_PR[$i]]['key']; ?>">
	<div class="card mb-3 related-posting">
		<div class="row align-items-center">
			<div class="col-md-12">
				<!-- 105 -->
				<img src="<?= $url.'imagens/informacoes/'.$vetKey[$vetKey_PR[$i]]['url']; ?>-1.jpg" alt="<?= $vetKey[$vetKey_PR[$i]]['key']; ?>" title="<?= $vetKey[$vetKey_PR[$i]]['key']; ?>">
				<!-- 205 -->
				<!-- <img src="<?= $url.'imagens/informacoes/300x220/'.$vetKey[$vetKey_PR[$i]]['url']; ?>-01.jpg" alt="<?= $vetKey[$vetKey_PR[$i]]['key']; ?>" title="<?= $vetKey[$vetKey_PR[$i]]['key']; ?>"> -->
			</div>
			<div class="col-md-12">
				<div class="card-body text-center  p-2">
					<h2 class="card-title text-center text-md-left" style="text-transform:uppercase;margin:0px"><?= $vetKey[$vetKey_PR[$i]]['key']; ?></h2>
				</div>
			</div>
		</div>
	</div></a>
<?php } ?>
</div>
</div>