<section class="video-section section-default">   
    <div class="h-100 w-100 vcont">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center text-white"> 
                    <h3 class="text-white">BLOCO DE ESPUMA INJETADA</h3>
                    <p>O bloco de espuma injetada, ao contrário da laminada, já é fabricada na medida exata da peça. Há uma Matriz (molde em aço no formato da espuma) que recebe o composto que formará a espuma pelo maneira de Injeção.</p>
                </div>
            </div>
        </div>
    </div>
</section>