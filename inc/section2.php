<div class="site-section block-13">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">  
                <h2 class="font-weight-bold text-black mb-4 h3 text-uppercase">Encontre os melhores fornecedores que você precisa</h2>
            </div>
        </div>
        <div class="nonloop-block-13 owl-carousel">
            <?php  
                $palavraEstudo_s8 = array(
                    'espuma casca de ovo',
                    'empresa de espuma',
                    'espuma de aglomerado',
                    'espuma de colchão',
                    'espuma em bloco',
                    'espuma flocada',
                    'espuma laminada',
                    'comprar espuma para colchão'
                );
                include_once('inc/vetKey.php');
                foreach ($vetKey as $key => $value) {
                    if(in_array(strtolower($value['key']), $palavraEstudo_s8)){
            ?> 
            <div class="col-12">
                <div class="col-12 post-entry p-0  bg-white" style="box-shadow: 0 5px 20px -5px rgba(0, 0, 0, 0.3);">
                    <a href="<?=$url.$value["url"];?>" class="d-block">
                        <!-- 105 -->
                        <img src="<?= $url.'imagens/informacoes/350x350/'.$value['url']; ?>-1.jpg" alt="<?=$value['key']?>" class="img-fluid" title="<?=$value['key']?>">
                        <!-- 205 -->
                        <!-- <img src="<?=$url?>images/img-mpi/350x350/<?= $value['url']; ?>-01.jpg" alt="<?=$value['key']?>" class="img-fluid" title="<?=$value['key']?>"> -->
                    </a>            
                    <h3 class="p-3 m-0 text-uppercase text-center d-flex justify-content-center align-items-center" style="min-height:76px"><a href="<?=$url.$value["url"];?>"><?=$value['key']?></a></h3>
                </div>
            </div>
            <?php } } ?>
        </div>
    </div>
</div>