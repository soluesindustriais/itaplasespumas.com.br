<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="font-weight-bold text-black mb-5 h3 text-uppercase">Você merece se sentir bem sorrindo!</h2>
            </div>
        </div>
        <div class="row ">
            <?php  
                $palavraEstudo_s8 = array(
                    'aparelho ortodôntico',
                    'cirurgia ortognática',
                    'clinica odontológica',
                    'gengiva inchada',
                    'mau hálito',
                    'periodontite',
                    'plano soluções industriais',
                    'consultório soluções industriais'
                );
                
                include_once('inc/vetKey.php');
                foreach ($vetKey as $key => $value) {
                    if(in_array(strtolower($value['key']), $palavraEstudo_s8)){
            ?> 
            <div class="col-lg-3 col-md-6 mb-4">
                <div class="col-12 post-entry p-0  bg-white" style="box-shadow: 0 0px 10px rgba(0, 0, 0, 0.25);">
                    <a href="<?= $url.$value["url"]; ?>" class="d-block">
                        <!-- 105 -->
                        <img src="<?=$url?>inc/thumbs.php?w=350&amp;h=350&amp;imagem=<?= $url.'imagens/informacoes/'.$value['url']; ?>-01.jpg" alt="<?=$value['key']?>" class="img-fluid" title="<?=$value['key']?>">
                        <!-- 205 -->
                        <!-- <img src="<?= $url; ?>images/img-mpi/350x350/<?= $value['url']; ?>-01.jpg" alt="<?= $value['key']; ?>" class="img-fluid" title="<?=$value['key']?>"> -->
                    </a>            
                    <h3 class="p-3 m-0 text-uppercase text-center d-flex justify-content-center align-items-center" style="min-height:76px"><a href="<?=$url.$value["url"];?>"><?=$value['key']?></a></h3>
                </div>
            </div>
            <?php } } ?>
        </div>
    </div>
</div>
