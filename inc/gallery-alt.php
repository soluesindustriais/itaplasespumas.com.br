<!-- gallery-105 -->
<div class="row d-flex justify-content-center">
    <?php
        for($i = 1; $i <= $quantia; $i++){
            $arquivojpg=dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."imagens/informacoes".DIRECTORY_SEPARATOR.$urlPagina."-".$i.".jpg";
            $arquivojpg0=dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."imagens/informacoes".DIRECTORY_SEPARATOR.$urlPagina."-0".$i.".jpg";
            $arquivopng=dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."imagens/informacoes".DIRECTORY_SEPARATOR.$urlPagina."-".$i.".png";
            $arquivopng0=dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."imagens/informacoes".DIRECTORY_SEPARATOR.$urlPagina."-0".$i.".png";

            if (file_exists($arquivojpg)) { $imagem=$url."imagens/informacoes/".$urlPagina."-".$i.".jpg"; }
            else if (file_exists($arquivojpg0)) { $imagem=$url."imagens/informacoes/".$urlPagina."-0".$i.".jpg"; }
            else if (file_exists($arquivopng)) { $imagem=$url."imagens/informacoes/".$urlPagina."-".$i.".png"; }
            else if (file_exists($arquivopng0)) { $imagem=$url."imagens/informacoes/".$urlPagina."-0".$i.".png"; }
            else { $imagem=$url."assets/img/logo-ok.png"; }
            
            //pegando a primeira imagem e guardando pra usar no scroll-popup
            if($i==1) $imgPopup = $imagem;
   ?>
    <div class="col-10 col-md-4" style="padding:5px">
        <a href="<?= $imagem; ?>" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>">
            <img src="<?=$url?>inc/thumbs.php?w=300&amp;h=220&amp;imagem=<?=$imagem?>" style="width:100%;padding:5px;height:100%;border:1px solid #eee;" alt="<?= $h1; ?>" />
        </a>
    </div>
    <?php } ?>
</div>
<!-- gallery-205 -->
<div class="carousel-mpi mb-3 row">
    <?php for($i = 1; $i <= $quantia; $i++){ ?>
    <div class="col-10 col-md-4" style="padding:5px">
        <a href="<?=$url?>images/img-mpi/<?=$urlPagina?>-<?=$i?>.jpg" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>">
            <img src="<?=$url?>images/img-mpi/300x220/<?=$urlPagina?>-<?=$i?>.jpg" style="width:100%;padding:5px;height:100%;border:1px solid #eee;" alt="<?= $h1; ?>" />
        </a>
    </div>
    <?php } ?>
</div>