<?php
$nomeSite = "Soluções Industriais ";
$slogan = "Espumas | Cote com dezenas de clientes";

$dir  = pathinfo($_SERVER['SCRIPT_NAME']);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/";  }
else { $url = $http."://".$host.$dir["dirname"]."/";  }

if ($_SERVER['HTTP_HOST'] != "localhost") $idAnalytics = "UA-121693798-25";

$ddd = "11";
$fone[0] = array("1111-1111","fone");
$fone[1] = array("2222-2222","whats");


$latitude = "-23.6531196";
$longitude = "-46.7226373";
$cidade = "São Paulo";
$uf = "SP";

$idCliente = "";
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
$creditos	= 'Soluções Industriais';
$tagsanalytic = ["G-8B4K2HGZM3","G-D23WW3S4NC"];

$explode	= explode("/", $_SERVER['PHP_SELF']);
$urlPagina = end($explode);
$urlPagina	= str_replace('.php','',$urlPagina);
$urlPagina == "index"? $urlPagina= "" : "";

//Gerar htaccess automático
$urlhtaccess = $url;
$schemaReplace = strpos($urlhtaccess, 'http://www.') === false ? 'http://' : 'http://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess,'/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/gerador-htaccess.php');

include ('inc/titletourl.php');
$prepos = array(' a ',' á ',' à ',' ante ',' até ',' após ',' de ',' desde ',' em ',' entre ',' com ',' para ',' por ',' perante ',' sem ',' sob ',' sobre ',' na ',' no ',' e ',' do ',' da ',' ','(',')','\'','"','.','/',':',' | ', ',, ');

$caminho ='
	<div class="breadcrumb" id="breadcrumb  ">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="" itemprop="item" title="Home">
			<span itemprop="name"> Home ❱  </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name"> '.$h1.'</span>
		  <meta itemprop="position" content="2" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	  ';
$caminho2 ='
	<div class="breadcrumb" id="breadcrumb  ">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="" itemprop="item" title="Home">
			<span itemprop="name"> Home ❱  </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="produtos" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name"></span>
		  <meta itemprop="position" content="3" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	';
$caminhoprodutos='
                    <div class="breadcrumb" id="breadcrumb">
                        <div class="wrapper">
                            <div class="bread__row">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                          <a href="" itemprop="item" title="Home">
                            <span itemprop="name"> Home ❱  </span>
                          </a>
                          <meta itemprop="position" content="1" />
                        </li>
                        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a  href="produtos-categoria" itemprop="item" title="Produtos - Categoria">
                          <span itemprop="name">Produtos - Categoria ❱  </span>
                        </a>
                        <meta itemprop="position" content="2" />
                      </li>
                        <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                          <span itemprop="name">'.$h1.'</span>
                          <meta itemprop="position" content="3" />
                        </li>
                      </ol>
                    </nav>
                    </div>
                    </div>
                    </div>
                    ';
?><? $isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]); ?>
