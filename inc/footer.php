<footer class="site-footer border-top py-4">
    <a href="#topo" class="scrollsuave" title="pra cima" alt="pra cima"><i class="fa fa-chevron-up text-center scrolltop"></i></a>
    <div class="container">
        <div class="row pt-3">
            <div class="col-md-6 col-5 text-center pb-4">
                <img src="<?= $url; ?>images/logo-branca.png" class="logo-footer" alt="Soluções Industriais ">
            </div>

            <div class="col-md-6 col-5 text-center pb-4">
                <img src="<?= $url; ?>images/logo-solucs.png" class="logo-footer-solucs" alt="Soluções Industriais ">
            </div>

            <div class="col-md-12  footer-nav ">
                <ul class="pages border-footer">
                    <li><a rel="nofollow" title="Home" href="<?= $url; ?>">Home</a></li>
                    <li><a rel="nofollow" title="Informações" href="<?= $url; ?>informacoes">Informações</a></li>
                    <li><a rel="nofollow" title="Quem somos" href="<?= $url; ?>quem-somos">Quem somos</a></li>
                    <li><a title="Mapa do site" href="<?= $url; ?>mapa-site">Mapa do site</a></li>
                    <li>
                        <a href="https://faca-parte.solucoesindustriais.com.br" target="_blank" class="nav-link theme-btn">Gostaria de anunciar?</a>
                    </li>
                </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <p>
                    Copyright &copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script> Todos os direitos reservados | Soluções Industriais
                </p>
            </div>
            <div class="col-md-6 selos text-center text-md-right">
                <a class="mr-2" rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank" title="HTML5 W3C">
                    <span class="fab fa-html5"></span>
                    <strong>W3C</strong></a>
                <a class="ml-2" rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&amp;lang=pt-BR" target="_blank" title="CSS W3C">
                    <span class="fab fa-css3"></span>
                    <strong>W3C</strong></a>
            </div>
        </div>
    </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->


<script src="<?= $url ?>js/bootstrap.min.js"></script>

<!-- Script Aside Launcher -->
<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<!-- Chat -->
<script src="//code.jivosite.com/widget/z7ft3dMscQ" async></script>

<!-- Launcher de exit -->
<div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />
<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>
<!-- Outros scripts -->
<? include "inc/LAB.php"?>

<!-- TagManager -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?= $idAnalytics; ?>"></script>
<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js" async></script>

<? include "inc/readmore.php"?>

<?php
foreach($tagsanalytic as $analytics){
    echo '<script async src="https://www.googletagmanager.com/gtag/js?id='.$analytics.'"></script>'.'<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}'."gtag('js', new Date()); gtag('config', '$analytics')</script>";
}
?>