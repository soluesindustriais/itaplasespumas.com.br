<div class="d-none" id="topo"></div>
<div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close">
            <span class="icon-close2 js-menu-toggle"><i class="fas fa-times"></i></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div>
<div class="sticky-top">
    <div class="site-navbar">
        <div class="container py-1">
            <div class="row align-items-center">
                <div class="col-8 col-md-2">
                    <a href="itaplasespumas.com.br"> <img class="site-logo lazyload"  data-src="<?= $url; ?>images/logo.png" alt="Soluções Industriais " /></a>
                </div>
                <div class="col-md-10 col-4">
                    <nav class="site-navigation text-right">
                        <div class="container">
                            <div class="d-inline-block d-lg-none ml-md-0 mr-auto"><a href="#" class="site-menu-toggle js-menu-toggle" title="logo"><span class="icon-menu fa fa-bars"></span></a></div>
                            <ul id="topnav" class="site-menu  js-clone-nav d-none d-lg-flex justify-content-around">
                                <li><a href="<?= $url; ?>">Home</a></li>
                                <li class="has-children">
                                    <a href="<?= $url; ?>informacoes">Informações</a>
                                    <ul class="dropdown arrow-top">
                                        <?php
                                        include('inc/vetKey.php');
                                        foreach ($vetKey as $key => $value) {
                                            echo '<li><a href="' . $url . $value["url"] . '">' . ucfirst($value['key']) . '</a></li>';
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <li class="has-children">
                                    <a href="<?= $url; ?>produtos-categoria">Produtos</a>
                                    <ul class="dropdown arrow-top">
                                        <?php
                                        include('inc/produtos/produtos-sub-menu.php');
                                        ?>
                                    </ul>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link menu-text" href="<?= $url ?>quem-somos" title="Quem Somos">Quem
                                        Somos</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link menu-text" href="<?=$url?>blog" title="Blog">Blog</a>
                                </li>
                                <li>
                                    <div class="bg-buscar">
                                        <input type="text" name="pesquisa" title="pesquisa" placeholder="Digite aqui o produto...">
                                        <button type="button" class="pesquisar"><img style="width:12px;" src="css/search-solid.svg" alt="Search"></button>
                                    </div>
                                </li>
                                <li>
                                    <a href="https://faca-parte.solucoesindustriais.com.br" target="_blank" target="_blank" class="nav-link theme-btn">Gostaria de anunciar?</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (($title != "Home") && ($pagInterna == "")) { ?>

<?php } ?>