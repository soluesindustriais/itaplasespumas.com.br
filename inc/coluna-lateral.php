
<aside class="menu-lateral col-md-3 col-12">
	<button title="<?= $h1?>" class="btn-cotar botao-cotar">Solicite um Orçamento</button>
    <h2 class="h5 bg-secondary rounded-0 text-white p-3 text-center text-uppercase mb-0">Produtos relacionados</h2>
    <nav>
 
        <ul class="list-group">
            <?php include('inc/vetKey.php');
            foreach ($vetKey as $key => $value) { ?>
            <li><a href="<?=$url.$value["url"];?>"  class="nav-link"><?=$value['key']?></a></li>
            <?php } ?>


        </ul>
    </nav>
</aside>

<hr class="mt-5">