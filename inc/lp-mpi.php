<div class="banner-titulo">
    <div class="h-100 w-100 d-flex align-items-center">
        <div class="container align-items-center">
            <div class="row">
                <div class="col-12">
                    <img src="<?= $url; ?>images/logo-texto.png" style="width:250px;" alt="Sempre perto de você"/>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 col-md-7">
            <?php $link = "http://servertemporario.com.br/guardiao/consultaideal/passos.xml";

            $xml = simplexml_load_file($link);
            foreach($xml as $item){  echo $item -> conteudo;  } ?>
        </div>
        <div class="col-12 col-md-5">
            <div class="contato" style="">
                <?php $link = "http://servertemporario.com.br/guardiao/consultaideal/formulario.xml";

                $xml = simplexml_load_file($link);
                foreach($xml as $item){  ?>
                <div class="fone">
                    <h2 class="titulo"><?= $item -> titulo; ?></h2>
                </div>
                <div class="formulario">
                    <?= $item -> campos; ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="w-100 mt-5 py-4" style="background:#E7E7E7">
    <div class="container">
        <div class="row esp">
            <div class="col-md-7 col-12">
                <?php $link = "http://servertemporario.com.br/guardiao/consultaideal/especialidades.xml";

                $xml = simplexml_load_file($link);
                foreach($xml as $item){  echo $item -> conteudo;  } ?>
            </div>
        </div>

    </div>
</div>
<div class="fixed-bottom d-none chamada p-3 col-md-3" style="left:auto;background:#E7E7E7;box-shadow: rgba(0, 0, 0, 0.25) 0px 0px 5px">
    <div class="row align-items-center">
        <div class="col-4">
            <!-- 105 -->
            <img src="<?= $url; ?>inc/thumbs.php?w=250&amp;h=150&amp;imagem=<?= $url.'imagens/informacoes/'.$urlPagina; ?>-01.jpg" class="w-100" alt="<?=$h1;?>">
            <!-- 205 -->
            <!-- <img src="<?= $url.'imagens/informacoes/250x150/'.$urlPagina; ?>-01.jpg" class="w-100" alt="<?=$h1;?>"> -->
        </div>
        <div class="col-8">
            <h5 class=" text-uppercase"><?=$h1;?></h5>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-12 text-center"><a href="#formulario" class="scrollsuave d-block btn-mpi py-1 w-100  text-center text-white">Solicite um Orçamento</a></div>
    </div>
</div>