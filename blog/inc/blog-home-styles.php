<?php
    $catHomeId = "1";

    $Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :st", "st=2");
    $homeCategories = $Read->getResult();
    
    if ($homeCategories):
        foreach($homeCategories as $key => $item):
            if($item['cat_id'] === $catHomeId):
                $catHomeTheme = $item['cat_home_theme'];
    
                switch($catHomeTheme){
                    case "Grid": include('css/blog-grid.css');
                        break;
                    case "List": include('css/blog-list.css');
                        break;
                    case "Full": include('css/blog-full.css');
                        break;
                    default:
                        break;
                }
            endif;
        endforeach;
    endif;
?>