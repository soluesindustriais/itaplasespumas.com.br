<?php
	if ($homeCategories):

		$blogMonthList = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
	
		$Read->ExeRead(TB_USERS, "WHERE user_status = :st", "st=0");
		$authors = $Read->getResult();

		foreach($homeCategories as $key => $item):
			if($item['cat_id'] === $catHomeId):
				$catHomeTheme = $item['cat_home_theme'];

				$Read = new Read;

				if($item['cat_parent']):
					$Read->ExeRead(TB_BLOG, "WHERE blog_status = :stats AND cat_parent = :cp ORDER BY blog_date DESC LIMIT 0, 3", "stats=2&cp=".$item['cat_id']);
				else:
					$Read->ExeRead(TB_BLOG, "WHERE blog_status = :stats ORDER BY blog_date DESC LIMIT 0, 6", "stats=2");
				endif;
				
				if($Read->getResult()):
					switch($catHomeTheme){
						case "Grid": include('doutor/layout/blog-grid/blog-home-grid.php');
							break;
						case "List": include('doutor/layout/blog-list/blog-home-list.php');
							break;
						case "Full": include('doutor/layout/blog-full/blog-home-full.php');
							break;
						default: 
							break;
					}
				else: ?>
					<div class="container">
						<div class="wrapper">
							<h2 class="text-center dark">Nenhum post encontrado</h2>
						</div>
					</div>
				<?php endif;
			endif;
		endforeach;
	else: ?>
		<div class="container">
			<div class="wrapper">
				<h2 class="text-center dark">Nenhuma categoria cadastrada</h2>
			</div>
		</div>
	<?php endif; 
?>