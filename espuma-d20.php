<? $h1 = "Espuma d20";
$title  = "Espuma d20";
$desc = "Encontre $h1, conheça as melhores indústrias, solicite um orçamento imediatamente com mais de 30 fabricantes ao mesmo tempo";
$key  = "Espuma d20,Espuma d20"; ?>
<!DOCTYPE html>
<html lang="pt-br">

<head> <?php include("inc/head.php"); ?>
    <link rel="stylesheet" href="<?= $url ?>css/style-mpi.css">
</head>

<body>
    <div class="site-wrap"> <?php include("inc/header-lista.php"); ?> <div class="container">
            <div class="row">
                <div class="col-12 mt-1"> <?php if (isset($pagInterna) && ($pagInterna != "")) {
                                                $previousUrl[0] = array("title" => $pagInterna);
                                            } ?> <?php include 'inc/breadcrumb.php' ?> </div>
                <div class="col-12 mt-3">
                    <h1 class="text-uppercase"> <?= $h1; ?> </h1>
                </div>
                <article class="col-md-9 col-12 text-black"> <?php $quantia = 3;
                                                                $j = 1;
                                                                include('inc/gallery.php'); ?>
                    <hr />
                    <h2>Espuma D20</h2>
                    <p>Um lançamento inovador com espuma d20 (densidade 20) de alta característica selado pelo Inmetro, atendendo à pedidos dos nossos clientes adequado sua ótima virtude e excelente preço.<br>Excelente a fim de uso esporádico (visitas) ou uso diário com intenção de crianças que estão se adaptando com a independência de dormir sem os pais, contando da mesma forma com o conforto e a praticidade ideal com o objetivo de toda sua família.<br>O modelo de Espuma d20 é indicado com intenção de o biotipo de pessoas que tenham o peso de até 50 quilos.<br> </p>
                    <p>Veja também <a target='_blank' title='Bloco de espuma d28' href="https://www.itaplasespumas.com.br/bloco-de-espuma-d28" style='cursor: pointer; color: #006fe6;font-weight:bold;'>Bloco de espuma d28</a>, e solicite agora mesmo uma <b>cotação gratuita</b> com um dos fornecedores disponíveis!</p>
                    <button title="<?= $h1 ?>" class="botao-cotar btn-cotar w-100">Solicite um Orçamento</button>

                </article> <?php include('inc/coluna-lateral.php'); ?> <?php include('inc/paginas-relacionadas.php'); ?> <?php include('inc/regioes.php'); ?> <?php include('inc/copyright.php'); ?>
            </div>
        </div><?php include("inc/footer.php"); ?> </div>
</body>

</html>