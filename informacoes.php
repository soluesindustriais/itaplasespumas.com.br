<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php 
    $h1 = "Informações";
    $title = "informacoes";
    $desc = "Referência no tratamento de sorrisos, Confira informações sobre nossos serviços prestados nas áreas de Ortodontia e outros - Clínica Ideal..."; 
    include("inc/head.php"); ?>     
</head>
<body>
    <div class="site-wrap">
        <?php include("inc/header-lista.php"); ?>
        <?= $caminho ?>
        <div class="container mb-4">
            <div class="row" id='paginacao'>
                <?php
                    include_once('inc/vetKey.php');
                    foreach ($vetKey as $key => $value) {
                ?>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="col-12 post-entry p-0  bg-white" style="box-shadow: 0 0px 10px rgba(0, 0, 0, 0.25);">
                        <a href="<?=$url.$value["url"];?>" class="d-block">
                            <!-- 105 -->
                            <img src="<?=$url.'imagens/informacoes/350x350/'.$value['url']; ?>-1.jpg" alt="<?=$value['key']?>" class="img-fluid" title="<?=$value['key']?>">
                            <!-- 205 -->
                            <!-- <img src="<?=$url?>images/img-mpi/350x350/<?=$value['url']?>-01.jpg" alt="<?=$value['key']?>" class="img-fluid" title="<?=$value['key']?>"> -->
                        </a>            
                        <h3 class="p-3 m-0 text-uppercase text-center d-flex justify-content-center align-items-center" style="min-height:76px"><a href="<?=$url.$value["url"];?>"><?=$value['key']?></a></h3>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php include("inc/footer.php"); ?>
    </div>
</body>
</html>