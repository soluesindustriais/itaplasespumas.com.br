<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php 
    $h1 = "Serviços emergenciais 24 horas";
    $title = "Home";
    $desc = "Serviços emergenciais 24 horas para colocar a sua saúde sempre em evidência. Tratamentos de canal, prótese, obturações, limpezas, extrações de siso e muito mais";
    include("inc/head.php"); ?>     
  </head>
  <body>
  <div class="site-wrap">
    <h1 class="d-none"><?= $h1; ?></h1>
    <?php include("inc/header-lista.php"); ?>
    <?php include("inc/slider.php"); ?>
    <?php include("inc/section2.php"); ?>
    <?php include("inc/section3.php"); ?>
    <?php include("inc/section1.php"); ?>
    <?php include("inc/footer.php"); ?>
  </div>    
  </body>
</html>